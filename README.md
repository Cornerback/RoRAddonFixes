Some small fixes to make the QueueQueuer addon work with the latest added Return of Reckong scenario. 

----------------

Here is how to install it:

    * Go to your Addons folder
    * Extract the QueueQueue.zip to your addons folder
    * Overwrite existing files
    * If game is running, do /rel ingame

-----------------

2.9.2 - Updated as of 12/23/2022.
* Added "The Old Dwarf Road" Scenario

2.9.1 - Updated as of 11/18/2022.
* Added "The Sacellum Arena" Pickup Scenario
* Included "Extra checks added to stop the settings file from being corrupted when you upgrade the add-on." by Zomega

2.8.11 - Updated as of 09/05/2022.
* Added "Discordant Skirmish" Pickup Scenario
* Removed "Tier 1-4" Tabs in options, as they provide no additional value in RoR but create extensive maintenance effort
* Fixed error in addon-naming so Warboardtoggler works again 

2.8.10 - Updated as of 06/10/2022.
* Added "Garden of Morr" Scenario

2.8.9 - Updated as of 03/11/2022.
* Added "Pyramid of Settra" Scenario

2.8.8 - Updated as of 11/01/2020.
* Added "DaemonBall Rally" Scenario
* Deleted specific Ranked scenarios from tab cause ranked and ranked solo are now generic sc entries 

2.8.7 - Updated as of 8/12/2020.
* Added "Ranked" & "Ranked Solo" Scenario
* Removed R/RS replacement for the "Ranked" & "Ranked Solo" SC

2.8.6 - Updated as of 6/26/2020.
* Re-Organized and enlarged T4 Tab (by Cornerback)

2.8.5 - Updated as of 5/01/2020.
* Fixed the "Caledor Woods (Ranked Solo)" scenario 

2.8.4 - Updated as of 5/01/2020.
* Merged changes made in other modified QueuerQueuer versions and changed version number accordingly
* Fixed Contextmenu position when not using warboard (again...)

2.7.4 - Updated as of 4/19/2020.
* "Maw of Madness (Ranked)" Scenario is now handled correctly
* Replace "Ranked" with R in the blacklist pop to avoid cutting off the name

2.7.3 - Updated as of 4/19/2020.
* Replace "Ranked Solo" with RS in the blacklist pop to avoid cutting off the name

2.7.2 - Updated as of 4/18/2020.
* "Maw of Madness (Ranked Solo)" Scenario is now handled correctly

2.7.1 - Updated as of 4/4/2020.
* Fixed blacklist and options parameter
* Fixed Contextmenu position when not using warboard
* "Thanquol's Stand: Ratnarök" Scenario is now handled correctly
* Update Warboard QueuerQueuer Contectmenu
